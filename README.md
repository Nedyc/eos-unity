# Epic Online Services - EOS for Unity

* From the official Online Multiplayer BaaS Discord Community @ https://discord.me/baas #eos-unity
* Unofficial EOS Unity implementation that "just works" out of the box.
* Vanilla backend implementation of the working EOS C# SDK architecture >> init >> tick >> create >> login.
* I went through the trial + error so you don't have to!

## Notes
* Tested in Unity 2020 with .NET 4.x scripting level
* Currently utilizing the 64-bit Windows DLL: If you have 32-bit support, feel free to throw that alongside the 64-bit one from the SDK zip (not included within this project).
* In the link above, also note the "platform define symbols": If you don't, I'll see you in the Troubleshooting section below, later ;)
* Don't forget to UPDATE your SDK! Just extract it using the same schema/architecture.
* Apparently (for new accounts only...?), some users only see 1 sandbox secret: Just use "live", in that case. Don't forget to change your EOSMgr component's 'SandboxStage' dropdown to 'Live', in that case (default==dev).

## Pre-Unity Instructions
1. You should probably familiarize yourself with the init flow @ https://dev.epicgames.com/docs/services/en-US/CSharp/GettingStarted/index.html
2. Login to https://dev.epicgames.com/portal/
3. You need to prepare **7** secrets and can be obscure to find:
    1. Create product
    2. Click game on left
    3. Product settings @ top-right.
    4. At the bottom, you must click an obscure link to an additional Terms of Service you need to accept before all clients will display (at the bottom >> "Client Details"):
    ![Screenshot](https://i.imgur.com/DaQfT0f.png)
    5. Prepare secrets:
        1. Product ID @ top under "General Information" | https://i.imgur.com/5lagSOf.png
	      2. 3x Sandbox IDs under "Sandboxes" for Live, Dev, and Staging | https://i.imgur.com/flChkFj.png
	      3. Beside 1 sandbox (probably Dev) under "Deployments" btn >> Create new deployment | https://i.imgur.com/pyNjjcQ.png
	      4. "Client Credentials" >> "New Client" btn @ bottom | https://i.imgur.com/PWrJ9I7.png
	
## Unity Instructions
1. Open project within Unity >> 'Yes' to auto-convert to your Unity version, if asked.
2. Swap out secret placeholders @ `/_Scripts/EOS/EOSInit.cs` (at the top):
![Secret Placeholders](https://i.imgur.com/MTTIPno.png)
3. Open StartScene >> Configure your `SandboxStage`:
![EOSMgr Sandbox Stage Dropdown](https://i.imgur.com/xx1slHQ.png)
4. Press Play >> Watch console and notice a portal login popup in default browser.
5. Notice your EOS dev portal ( https://dev.epicgames.com/portal ) analytics section starting to fill with data!
![Success Logs](https://i.imgur.com/3LyuZsq.png)

## Troubleshooting
1. **NOT using 64-bit editor/standalone?** Go back to Instructions #1's link and scroll down to "Unity Integration" regarding *Platform Define Symbols* -- you'll need to change something.

2. **DLLNotFound Error?** Same issue as #1: If you'are on 64-bit but still get this error, did you copy+paste this template to your own project? *Don't forget to add those definition symbols within the instructional link from #1!

3. **ResultCode==UnexpectedError** This is an Epic unhandled err for most-likely saying you messed up your keys. See instruction #3 for swapping out secret placeholders.

## Included Libs
This near-vanilla project does require 1 lib due to lack of time when minimizing from my own project:

More-Effective Coroutines (MEC), which is free and already included within the project @ 1. More-Effective Coroutines (MEC) by Trinary Software (FREE) @ https://assetstore.unity.com/packages/tools/animation/more-effective-coroutines-free-54975

This basically just utilizes .WaitUntilDone() and .cancelWith() yield features. Feel free to add a push request just adding normal coroutines for the crazies that still use Unity's native [lacking] coroutines ;) for the sake of being minimal, anyway.

## Fork it!
* How about a fork that includes friends lists?
* How about a fork that focuses on analytics events?
* How about some ultra-friendly wrapper that combines EVERYTHING together?
* Let's see what you come up with!

## Feel Warm and Fuzzy?
* Let's network! https://www.linkedin.com/in/dylanhuntdev
* Happy with what you see? Buy me a beer by supporting my game, [Throne of Lies: The Online Game of Deceit](https://toli.es/steam) (on Steam).
--Xblade
